﻿namespace LinqAggregateOperators
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var samples = new LinqSamples();

            // This sample uses Count to get the number of unique prime factors of 300
            samples.Linq73();

            // This sample uses Count to get the number of odd ints in the array
            samples.Linq74();

            // This sample uses Count to return a list of customers and how many orders each has
            samples.Linq76();

            // This sample uses Count to return a list of categories and how many products each has
            samples.Linq77();

            // This sample uses Sum to add all the numbers in an array
            samples.Linq78();

            // This sample uses Sum to get the total number of characters of all words in the array
            samples.Linq79();

            // This sample uses Sum to get the total units in stock for each product category
            samples.Linq80();

            // This sample uses Min to get the lowest number in an array
            samples.Linq81();

            // This sample uses Min to get the length of the shortest word in an array
            samples.Linq82();

            // This sample uses Min to get the cheapest price among each category's products
            samples.Linq83();

            // This sample uses Min to get the products with the lowest price in each category
            samples.Linq84();

            // This sample uses Max to get the highest number in an array. Note that the method 
            // returns a single value.
            samples.Linq85();

            // This sample uses Max to get the length of the longest word in an array
            samples.Linq86();

            // This sample uses Max to get the most expensive price among each category's products
            samples.Linq87();

            // This sample uses Max to get the products with the most expensive price in each category
            samples.Linq88();

            // This sample uses Average to get the average of all numbers in an array
            samples.Linq89();

            // This sample uses Average to get the average length of the words in the array
            samples.Linq90();

            // This sample uses Average to get the average price of each category's products
            samples.Linq91();

            // This sample uses Aggregate to create a running product on the array that calculates 
            // the total product of all elements.
            samples.Linq92();

            // This sample uses Aggregate to create a running account balance that subtracts each 
            // withdrawal from the initial balance of 100, as long as the balance never drops below
            // zero.
            samples.Linq93();
        }
    }
}
