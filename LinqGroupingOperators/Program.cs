﻿namespace LinqGroupingOperators
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var samples = new LinqSamples();

            // This sample uses group by to partition a list of numbers by their remainder when
            //divided by 5.
            samples.DataSetLinq40();

            // This sample uses group by to partition a list of words by their first letter.
            samples.DataSetLinq41();

            // This sample uses group by to partition a list of products by category.
            samples.DataSetLinq42();

            // This sample uses group by to partition a list of each customer's orders, first by
            // year, and then by month.
            samples.DataSetLinq43();

            // This sample uses GroupBy to partition trimmed elements of an array using a custom
            // comparer that matches words that are anagrams of each other.
            samples.DataSetLinq44();

            // This sample uses GroupBy to partition trimmed elements of an array using a custom
            // comparer that matches words that are anagrams of each other, and then converts the
            // results to uppercase.
            samples.DataSetLinq45();
        }
    }
}
